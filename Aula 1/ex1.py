import matplotlib.pyplot as plt
import numpy as np
from scipy import integrate

def rect(x):
    return np.where(abs(x)<=0.5, 1, 0)

N = 10000
a = -20
b = 20
t = np.linspace(a,b,N+1,dtype=np.complex128)
f = np.linspace(a,b,N+1,dtype=np.complex128)
yw = np.zeros(N+1, dtype=np.complex128)

for x in range (0, N):
    yw[x] = integrate.simps(rect(t)*np.exp(-1*np.complex(0,1)*2*np.pi*t*f[x]),t)

plt.figure()
plt.plot(f, np.abs(yw))  
plt.grid(True)
plt.xlabel('$f$');
plt.ylabel('$Y(f)$');
plt.show()