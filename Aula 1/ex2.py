import matplotlib.pyplot as plt
import numpy as np
import scipy.fftpack

def rct(t):
    return (abs(t) < 0.5).astype(float)

t0 = 4
taxa = 5
amostras = 2*t0*taxa
t  = np.linspace(-t0, t0, amostras)

f  = (rct(t) - rct(t-0.5)) * np.exp(-t*t)  # define the function

fw = scipy.fftpack.fft(f)

freq = np.arange(0,len(fw))*taxa/amostras

print(f.shape)
print(fw.shape)

plt.figure()
plt.plot(freq, fw)  
plt.grid(True)
plt.ylim((-1.1,1.1));
plt.xlabel('$t$');
plt.ylabel('$g(t)$');
plt.show()

